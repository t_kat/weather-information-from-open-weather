"""
title:
    open_weather.py

description:
    fetch weather report from open weather API.

License:
    Copyright (c) 2017 t.kat
    This module is released under the MIT License.
"""
# -*- coding:utf-8 -*-

import sys
import json

from urllib.request import urlopen
from bs4 import BeautifulSoup

URL_PREFIX = "http://api.openweathermap.org/data/2.5/weather?q="
APPID = "open_weather_api_key"
KEY_DICT = {"city": "name", "weather": ["weather", "main"],
            "temperture": ["main", "temp"], "temp_max": ["main", "temp_max"],
            "temp_min": ["main", "temp_min"], "humidity": ["main", "humidity"],
            "condition":["weather", "description"], "icon":["weather","icon"],
            "wind_speed":["wind", "speed"], "wind_direction":["wind", "deg"],
            "clouds": ["clouds", "3h"], "rain": ["rain", "3h"], "snow":["snow", "3h"]}

class OpenWeather(object):
    def __init__(self):
        self.weather_statics = dict()

    def fetch_weather_data(self, country, city):
        print(URL_PREFIX + city.capitalize() + "," + country.lower() + "&appid=" + APPID)
        data = urlopen(URL_PREFIX + city.capitalize() + "," + country.lower() + "&appid=" + APPID)
        data = BeautifulSoup(data).findAll("p")[0].get_text()
        data = self._convert_json_to_dict(data)

        print(data)
        return self._generate_weather_statics(data)

    def _generate_weather_statics(self, data):
        """ APIから引っ張ってきた素のjsonデータを変換した辞書では使いづらいので、
            使いやすいようにセットし直す。

        """
        for k, v in KEY_DICT.items():
            try:
                self.weather_statics[k] = self.__extract(data, v)
            except KeyError:
                pass

        self.weather_statics = self.__convert_f2c(self.weather_statics)
        return self.weather_statics

    def _convert_json_to_dict(self, data):
        """ json文字列を辞書に変換

        """
        return json.loads(data)

    def __extract(self, data, key):
        ''' 深さのあるjsonデータを抽出する（例："main":[{"sub":"ABC", "sub2":"DEF"}], "main2":"HIJ"）

            __extract(json, ["main", "sub"])
            >> "ABC"
            深さ1なら文字列でもOK!
            __extract(json, "main2")
            >> "HIJ"
        '''
        if (len(key) == 1 and type(key) is list) or type(key) is str:
            return data[key]
        else:
            tmp = data
            for k in key:
                try:
                    if type(tmp[k]) is list:
                        tmp = tmp[k][0]
                    else:
                        tmp = tmp[k]
                except KeyError as err:
                    raise err
            return tmp

    def __convert_f2c(self, data):
        kelvin = 273.15
        data["temp_min"] = round(data["temp_min"] - kelvin, 2)
        data["temp_max"] = round(data["temp_max"] - kelvin, 2)
        data["temperture"] = round(data["temperture"] - kelvin, 2)

        return data

if __name__ == "__main__":
    o = OpenWeather()
    o.fetch_weather_data(sys.argv[1], sys.argv[2])
    print(o.weather_statics)
